"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pad2Length = exports.keyFromParams = exports.getParamsFromKey = exports.sleep = exports.info = void 0;
function info() {
    return 'SafeInvest base library';
}
exports.info = info;
async function sleep(timeout) {
    return new Promise(resolve => setTimeout(() => resolve(), timeout));
}
exports.sleep = sleep;
function getParamsFromKey(key) {
    const params = key.split(':');
    return {
        seType: params[0],
        se: params[1],
        symbol: params[2],
        interval: params[3],
        entityType: params[4],
        options: params[5],
        key,
    };
}
exports.getParamsFromKey = getParamsFromKey;
function keyFromParams(params) {
    return `${params.seType}:${params.se}:${params.symbol}:${params.interval}:${params.entityType}:${params.options}`;
}
exports.keyFromParams = keyFromParams;
function pad2Length(src = '', length = 10) {
    return src + ' '.repeat(length - src.length);
}
exports.pad2Length = pad2Length;
//# sourceMappingURL=SiLib.js.map