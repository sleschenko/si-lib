export declare class Candle {
    closeTime: Date;
    openTime: Date;
    open: number;
    high: number;
    low: number;
    close: number;
    volume: number;
    qty: number;
    growing: boolean;
    constructor(str: string);
}
