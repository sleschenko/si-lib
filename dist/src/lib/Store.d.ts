import { RedisClient } from 'redis';
export declare type RedisEventHandler = (msg: string) => void;
export interface RedisConfig {
    host: string;
    port: number;
}
export declare class Store {
    protected readonly client: RedisClient;
    private readonly redisSubscriber;
    private eventHandlers;
    constructor(config: RedisConfig);
    stop(): void;
    registerEventHandler(handler: RedisEventHandler): number;
    private onMessage;
    sendEvent(message: string): void;
    llen(key: string): Promise<number>;
    deleteKey(key: string): Promise<number>;
    /**
     * Get keys functions
     */
    getKeysScan(template: string): Promise<string[]>;
    getKeys(template: string): Promise<string[]>;
    private scan;
    /**
     * Get specified keys
     */
    candlesKeys(): Promise<string[]>;
    candlesBinanceKeys(): Promise<string[]>;
    indicatorsKeys(): Promise<string[]>;
    /**
     * Values functions
     */
    getValues(key: string, start?: number, stop?: number): Promise<string[]>;
    pushValues(key: string, list: string[], entityName?: string): Promise<number>;
    popValue(key: string): Promise<string>;
    setValue(key: string, index: number, value: string, entityName?: string): Promise<string>;
}
