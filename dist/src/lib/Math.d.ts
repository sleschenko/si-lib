export declare function safePlus(x: number, y: number): number;
export declare function safeMinus(x: number, y: number): number;
export declare function safeDiv(x: number, y: number): number;
export declare function safeMult(x: number, y: number): number;
export declare function addPercentToValue(value: number, percent: number): number;
export declare function toFitPrecession(value: number, precession: number): number;
