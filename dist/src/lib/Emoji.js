"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Emoji = void 0;
exports.Emoji = {
    elephant: '🐘',
    rocket: '🚀,',
    up_1: '✅',
    down_1: '🔻',
    yellow_circle: '🟡',
    green_circle: '🟢',
    red_circle: '🔴',
    magenta_circle: '🟣',
    blue_circle: '🔵',
    orange_circle: '🟠',
    redExclamationMark: '❗️',
    buttArrowUp: '⬆',
    buttArrowDown: '⬇',
};
//# sourceMappingURL=Emoji.js.map