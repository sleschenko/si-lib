export declare type BaseKey = string | number;
export declare type BaseDict<T = string> = {
    [key in BaseKey]: T;
};
export declare type StockExchangeType = 'c' | 'f';
export declare type StockExchange = 'binance' | 'moex' | 'tsc';
export declare type EntityType = 'candle' | 'open' | 'indicator';
export declare type CandleInterval = '1m' | '10m' | '1h' | '4h' | '1d' | '1w' | '1M';
export interface CandleParams {
    seType: StockExchangeType;
    se: StockExchange;
    symbol: string;
    interval: CandleInterval;
    entityType: EntityType;
    options: string;
    key: string;
}
export declare const intervalMillis: {
    [key in CandleInterval]: number;
};
