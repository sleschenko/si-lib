import { CandleParams } from './Types';
export declare function info(): string;
export declare function sleep(timeout: number): Promise<void>;
export declare function getParamsFromKey(key: string): CandleParams;
export declare function keyFromParams(params: CandleParams): string;
export declare function pad2Length(src?: string, length?: number): string;
