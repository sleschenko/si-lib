"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Candle = void 0;
class Candle {
    constructor(str) {
        try {
            const c = JSON.parse(str);
            this.closeTime = new Date(c.ct);
            this.openTime = new Date(c.ot);
            this.open = c.o;
            this.high = c.h;
            this.low = c.l;
            this.close = c.c;
            this.volume = c.v;
            this.qty = c.q;
            this.growing = this.open < this.close;
        }
        catch (e) {
            throw new Error('bad candle params!');
        }
    }
}
exports.Candle = Candle;
//# sourceMappingURL=Candle.js.map