import { Candle } from '../Candle';
import { IndicatorInterface, IndicatorBase } from './types';
export declare class IndicatorRSI extends IndicatorBase {
    constructor(data: string);
}
export declare class RSI {
    static calc(list: Candle[], depth: number): IndicatorInterface[];
    private static calcRSIFull;
    private static calcRSIRecurrent;
}
