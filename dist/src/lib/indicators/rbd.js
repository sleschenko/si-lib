"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RBD = exports.IndicatorRBD = void 0;
const types_1 = require("./types");
class IndicatorRBD extends types_1.IndicatorBase {
    constructor(data) {
        super(data);
        this.value = +this.raw.rsiType;
    }
}
exports.IndicatorRBD = IndicatorRBD;
class RBD {
    static calc(candles, depthRSI = 14, depthBasis = 20, devCount = 2, sigma = .1) {
        const indicator = candles.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            src: candle.close,
            rsi: 0,
            sma: 0,
            ema: 0,
            lineUp: 0,
            lineDown: 0,
            dispDown: 0,
            dispUp: 0,
            rsiType: 0,
            value: 0,
        }));
        if (candles.length < depthRSI || candles.length < depthBasis * 2) {
            return indicator;
        }
        this.calcRSI(indicator, depthRSI);
        this.calcEMA(indicator.slice(depthBasis * 2), depthBasis, devCount, sigma);
        return indicator;
    }
    static calcEMA(list, depth, devCount, sigma) {
        const alpha = 2 / (depth + 1);
        const stdDev = (base) => {
            const ma = base.reduce((a, c) => a + c.rsi, 0) / base.length;
            const sum = base.reduce((a, c) => a + Math.pow(c.rsi - ma, 2), 0);
            return Math.sqrt(sum / base.length);
        };
        const prevAvg1 = list
            .slice(0, depth - 1)
            .reduce((a, c) => a + c.rsi, 0) / depth;
        list[depth - 1].ema = prevAvg1;
        let dev = devCount * stdDev(list.slice(0, depth));
        list[depth - 1].lineDown = prevAvg1 - dev;
        list[depth - 1].lineUp = prevAvg1 + dev;
        let prevEMA = prevAvg1;
        for (let q = depth; q < list.length; q++) {
            const curRSI = list[q].rsi;
            const curEMA = prevEMA * (1 - alpha) + curRSI * alpha;
            list[q].ema = curEMA;
            prevEMA = curEMA;
            dev = devCount * stdDev(list.slice(q + 1 - depth, q + 1));
            const lineUp = curEMA + dev;
            const lineDown = curEMA - dev;
            list[q].lineUp = lineUp;
            list[q].lineDown = lineDown;
            const dispUp = curEMA + (lineUp - lineDown) * sigma;
            const dispDown = curEMA - (lineUp - lineDown) * sigma;
            list[q].dispUp = dispUp;
            list[q].dispDown = dispDown;
            if (list[q].rsi > list[q].dispUp) {
                list[q].rsiType = 1;
            }
            else if (list[q].rsi < list[q].dispDown) {
                list[q].rsiType = -1;
            }
        }
        return list;
    }
    static calcRSI(list, depth) {
        let firstGain = 0;
        let firstLose = 0;
        for (let e = 0; e < depth; e++) {
            const currentPrice = list[e + 1].src;
            const prevPrice = list[e].src;
            if (currentPrice >= prevPrice) {
                firstGain += currentPrice - prevPrice;
            }
            else {
                firstLose += prevPrice - currentPrice;
            }
        }
        firstGain /= depth;
        firstLose /= depth;
        const firstRS = firstGain / firstLose;
        list[depth].rsi = 100 - 100 / (1 + firstRS);
        for (let q = depth + 1; q < list.length; q++) {
            const currentPrice = list[q].src;
            const prevPrice = list[q - 1].src;
            let currentGain = 0;
            let currentLose = 0;
            if (currentPrice >= prevPrice) {
                currentGain = currentPrice - prevPrice;
            }
            else {
                currentLose = prevPrice - currentPrice;
            }
            currentGain = (firstGain * (depth - 1) + currentGain) / depth;
            currentLose = (firstLose * (depth - 1) + currentLose) / depth;
            const currentRS = currentGain / currentLose;
            list[q].rsi = 100 - 100 / (1 + currentRS);
            firstGain = currentGain;
            firstLose = currentLose;
        }
        return list;
    }
}
exports.RBD = RBD;
//# sourceMappingURL=rbd.js.map