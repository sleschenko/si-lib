import { Candle } from '../Candle';
import { IndicatorInterface, IndicatorBase } from './types';
export interface IndicatorStochasticInterface extends IndicatorInterface {
    k: number;
    d: number;
}
export declare class IndicatorStochastic extends IndicatorBase implements IndicatorStochasticInterface {
    k: number;
    d: number;
    constructor(data: string);
}
export declare class Stochastic {
    static calc(list: Candle[], periodK?: number, periodD?: number, smoothK?: number, lazy?: boolean): IndicatorStochasticInterface[];
    private static calcIndex;
    private static calcSMA;
}
