"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPriceFromType = exports.getIndicatorParams = exports.IndicatorBase = void 0;
class IndicatorBase {
    constructor(data) {
        this.raw = JSON.parse(data);
        const { ot, ct, value } = JSON.parse(data);
        this.ot = new Date(ot);
        this.ct = new Date(ct);
        this.value = +value;
    }
}
exports.IndicatorBase = IndicatorBase;
function getIndicatorParams(params) {
    const ps = params.options.split('-');
    return {
        name: ps[0],
        params: ps.slice(1),
    };
}
exports.getIndicatorParams = getIndicatorParams;
function getPriceFromType(priceType, candle) {
    switch (priceType) {
        case 'hlc3':
            return (candle.high + candle.low + candle.close) / 3;
        default:
            return candle[priceType];
    }
}
exports.getPriceFromType = getPriceFromType;
//# sourceMappingURL=types.js.map