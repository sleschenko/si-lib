import { IndicatorInterface, MAType, PriceType } from './types';
import { Candle } from '../Candle';
export interface IndicatorMACDInterface extends IndicatorInterface {
    macd: number;
    signal: number;
    diffs: number;
    close?: number;
}
export declare class MACD {
    static calc(candles: Candle[], periodFast: number, periodSlow: number, signalLength: number, priceType?: PriceType, maType?: MAType, signalMAType?: MAType): IndicatorMACDInterface[];
    static padIndicatorList(src: IndicatorInterface[], padTo: number): IndicatorInterface[];
}
