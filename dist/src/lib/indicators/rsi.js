"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RSI = exports.IndicatorRSI = void 0;
const types_1 = require("./types");
class IndicatorRSI extends types_1.IndicatorBase {
    constructor(data) {
        super(data);
    }
}
exports.IndicatorRSI = IndicatorRSI;
class RSI {
    static calc(list, depth) {
        const rsi = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: 0,
        }));
        return this.calcRSIRecurrent(list, rsi, depth);
    }
    static calcRSIFull(list, rsi, indexDepth) {
        for (let q = list.length; q > indexDepth; q--) {
            const { grow, lose } = list
                .slice(q - indexDepth, q)
                .reduce((a, c) => {
                if (c.growing) {
                    a.grow += c.close - c.open;
                }
                else {
                    a.lose += c.open - c.close;
                }
                return a;
            }, { grow: 0, lose: 0 });
            const RS = grow / lose;
            rsi[q - 1].value = 100 - 100 / (1 + RS);
        }
        return rsi;
    }
    static calcRSIRecurrent(list, rsi, indexDepth) {
        let firstGain = 0;
        let firstLose = 0;
        for (let e = 0; e < indexDepth; e++) {
            const currentPrice = list[e + 1].close;
            const prevPrice = list[e].close;
            if (currentPrice >= prevPrice) {
                firstGain += currentPrice - prevPrice;
            }
            else {
                firstLose += prevPrice - currentPrice;
            }
        }
        firstGain /= indexDepth;
        firstLose /= indexDepth;
        const firstRS = firstGain / firstLose;
        rsi[indexDepth].value = 100 - 100 / (1 + firstRS);
        for (let q = indexDepth + 1; q < list.length; q++) {
            const currentPrice = list[q].close;
            const prevPrice = list[q - 1].close;
            let currentGain = 0;
            let currentLose = 0;
            if (currentPrice >= prevPrice) {
                currentGain = currentPrice - prevPrice;
            }
            else {
                currentLose = prevPrice - currentPrice;
            }
            currentGain = (firstGain * (indexDepth - 1) + currentGain) / indexDepth;
            currentLose = (firstLose * (indexDepth - 1) + currentLose) / indexDepth;
            const currentRS = currentGain / currentLose;
            rsi[q].value = 100 - 100 / (1 + currentRS);
            firstGain = currentGain;
            firstLose = currentLose;
        }
        return rsi;
    }
}
exports.RSI = RSI;
//# sourceMappingURL=rsi.js.map