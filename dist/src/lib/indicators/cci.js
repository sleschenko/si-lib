"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CCI = void 0;
const types_1 = require("./types");
const sma_1 = require("./movings/sma");
class CCI {
    /**
     * calculating CCI index
     * src algo: https://ru.wikipedia.org/wiki/%D0%98%D0%BD%D0%B4%D0%B5%D0%BA%D1%81_%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BD%D0%BE%D0%B3%D0%BE_%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%D0%B0
     */
    static calc(list, depth, priceType = 'close') {
        const cci = list.map(item => ({
            ot: item.openTime,
            ct: item.closeTime,
            value: 0
        }));
        const prices = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: types_1.getPriceFromType(priceType, candle),
        }));
        const pricesSMA = sma_1.SMA.calcIndex(prices, depth);
        const MAD = (n) => {
            let sum = 0;
            for (let i = n - depth + 1; i <= n; i++) {
                sum += Math.abs(prices[i].value - pricesSMA[n].value);
            }
            return sum / depth;
        };
        for (let i = depth; i < prices.length; i++) {
            cci[i].value = (prices[i].value - pricesSMA[i].value) / (0.015 * MAD(i));
        }
        return cci;
    }
}
exports.CCI = CCI;
//# sourceMappingURL=cci.js.map