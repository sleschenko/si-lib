"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Stochastic = exports.IndicatorStochastic = void 0;
const types_1 = require("./types");
class IndicatorStochastic extends types_1.IndicatorBase {
    constructor(data) {
        super(data);
        this.k = +this.raw.k || 0;
        this.d = +this.raw.d || 0;
    }
}
exports.IndicatorStochastic = IndicatorStochastic;
class Stochastic {
    static calc(list, periodK = 14, periodD = 3, smoothK = 3, lazy = true) {
        if (periodK > list.length) {
            return [];
        }
        return this.calcIndex(list, periodK, periodD, smoothK, lazy);
    }
    static calcIndex(list, periodK, periodD, smoothK, lazy) {
        const stoch = [];
        for (let i = 1; i <= (lazy ? periodK : list.length); i++) {
            const index = list.length - i;
            const currentCandle = list[index];
            const [min, max] = list
                .slice(index - periodK + 1, index + 1)
                .reduce((a, c) => [
                c.low < a[0] ? c.low : a[0],
                c.high > a[1] ? c.high : a[1]
            ], [Number.MAX_VALUE, 0]);
            const value = (currentCandle.close - min) / (max - min);
            stoch.unshift({
                value,
                ot: currentCandle.openTime,
                ct: currentCandle.closeTime,
                k: 0,
                d: 0,
            });
        }
        this.calcSMA(stoch, 'k', smoothK);
        this.calcSMA(stoch, 'd', periodD);
        stoch.forEach(i => {
            i.k = Math.round(i.k * 10000) / 100;
            i.d = Math.round(i.d * 10000) / 100;
        });
        return stoch;
    }
    static calcSMA(src, fieldName, period) {
        const srcFieldName = fieldName === 'k' ? 'value' : 'k';
        for (let i = src.length; i >= period; i--) {
            src[i - 1][fieldName] = src
                .slice(i - period, i)
                .reduce((a, c) => a + c[srcFieldName], 0) / period;
        }
        return src;
    }
}
exports.Stochastic = Stochastic;
//# sourceMappingURL=stochastic.js.map