import { Candle } from '../Candle';
import { IndicatorInterface, PriceType } from './types';
export declare class CCI {
    /**
     * calculating CCI index
     * src algo: https://ru.wikipedia.org/wiki/%D0%98%D0%BD%D0%B4%D0%B5%D0%BA%D1%81_%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BD%D0%BE%D0%B3%D0%BE_%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%D0%B0
     */
    static calc(list: Candle[], depth: number, priceType?: PriceType): IndicatorInterface[];
}
