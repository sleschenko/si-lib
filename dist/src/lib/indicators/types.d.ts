import { BaseDict, CandleParams } from '../Types';
import { Candle } from '../Candle';
export declare type PriceType = 'open' | 'high' | 'low' | 'close' | 'hlc3';
export declare type MAType = 'SMA' | 'EMA' | 'HMA' | 'WMA';
export interface IndicatorParams {
    name: string;
    params: string[];
}
export interface IndicatorInterface {
    ot: Date;
    ct: Date;
    value: number;
}
export declare abstract class IndicatorBase implements IndicatorInterface {
    raw: BaseDict<string | number>;
    ot: Date;
    ct: Date;
    value: number;
    protected constructor(data: string);
}
export declare function getIndicatorParams(params: CandleParams): IndicatorParams;
export declare function getPriceFromType(priceType: PriceType, candle: Candle): number;
