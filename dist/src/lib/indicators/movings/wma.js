"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WMA = void 0;
const types_1 = require("../types");
class WMA {
    static calc(list, depth, priceType = 'close') {
        const prices = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: types_1.getPriceFromType(priceType, candle),
        }));
        return WMA.calcIndex(prices, depth);
    }
    static calcIndex(src, depth) {
        const wma = src.map(item => ({
            ot: item.ot,
            ct: item.ct,
            value: 0
        }));
        for (let i = depth; i <= src.length; i++) {
            wma[i - 1].value = (2 / (depth * (depth + 1))) * src
                .slice(i - depth, i)
                .reduce((a, c, num) => a + (num + 1) * c.value, 0);
        }
        return wma;
    }
}
exports.WMA = WMA;
//# sourceMappingURL=wma.js.map