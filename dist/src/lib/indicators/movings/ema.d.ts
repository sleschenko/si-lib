import { IndicatorBase, IndicatorInterface, PriceType } from '../types';
import { Candle } from '../../Candle';
export declare class IndicatorEMA3 extends IndicatorBase {
    emaFast: number;
    emaMiddle: number;
    emaSlow: number;
    constructor(data: string);
}
export declare class EMA {
    static calc(list: Candle[], depth: number, priceType?: PriceType): IndicatorInterface[];
    private static calcFirstSMA;
    private static calcIndex;
}
