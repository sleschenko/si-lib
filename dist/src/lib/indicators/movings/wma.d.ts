import { Candle } from '../../Candle';
import { IndicatorInterface, PriceType } from '../types';
export declare class WMA {
    static calc(list: Candle[], depth: number, priceType?: PriceType): IndicatorInterface[];
    static calcIndex(src: IndicatorInterface[], depth: number): IndicatorInterface[];
}
