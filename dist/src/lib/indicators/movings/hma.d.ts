import { Candle } from '../../Candle';
import { IndicatorInterface, PriceType } from '../types';
export declare class HMA {
    static calc(list: Candle[], depth: number, priceType?: PriceType): IndicatorInterface[];
    static calcIndex(src: IndicatorInterface[], depth: number): IndicatorInterface[];
}
