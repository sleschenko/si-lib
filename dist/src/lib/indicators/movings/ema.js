"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMA = exports.IndicatorEMA3 = void 0;
const types_1 = require("../types");
class IndicatorEMA3 extends types_1.IndicatorBase {
    constructor(data) {
        super(data);
        this.emaFast = +this.raw.emaFast;
        this.emaMiddle = +this.raw.emaMiddle;
        this.emaSlow = +this.raw.emaSlow;
    }
}
exports.IndicatorEMA3 = IndicatorEMA3;
class EMA {
    static calc(list, depth, priceType = 'close') {
        const src = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: types_1.getPriceFromType(priceType, candle),
        }));
        const ema = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: 0,
        }));
        if (depth > list.length) {
            return ema;
        }
        return EMA.calcIndex(src, ema, depth);
    }
    static calcFirstSMA(src, depth) {
        return src
            .slice(0, depth)
            .reduce((a, c) => a + c.value, 0) / depth;
    }
    static calcIndex(src, ema, depth) {
        const alpha = 2 / (depth + 1);
        ema[depth - 1].value = this.calcFirstSMA(src, depth);
        for (let i = depth; i < src.length; i++) {
            ema[i].value = alpha * src[i].value + (1 - alpha) * ema[i - 1].value;
        }
        return ema;
    }
}
exports.EMA = EMA;
//# sourceMappingURL=ema.js.map