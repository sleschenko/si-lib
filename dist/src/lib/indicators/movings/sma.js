"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SMA = void 0;
const types_1 = require("../types");
class SMA {
    static calc(list, depth, priceType = 'close') {
        const src = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: types_1.getPriceFromType(priceType, candle),
        }));
        return SMA.calcIndex(src, depth);
    }
    static calcIndex(src, depth) {
        const sma = src.map(item => ({
            ot: item.ot,
            ct: item.ct,
            value: 0,
        }));
        for (let i = 0; i < src.length; i++) {
            if (i < depth) {
                sma[depth - 1].value += src[i].value / depth;
            }
            else {
                const prevSMA = sma[i - 1].value;
                const currValue = src[i].value;
                const firstDepthValue = src[i - depth].value;
                sma[i].value = prevSMA - firstDepthValue / depth + currValue / depth;
            }
        }
        return sma;
    }
}
exports.SMA = SMA;
//# sourceMappingURL=sma.js.map