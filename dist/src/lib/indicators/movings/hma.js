"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HMA = void 0;
const types_1 = require("../types");
const wma_1 = require("./wma");
class HMA {
    static calc(list, depth, priceType = 'close') {
        const prices = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: types_1.getPriceFromType(priceType, candle),
        }));
        return HMA.calcIndex(prices, depth);
    }
    static calcIndex(src, depth) {
        const hmaSrc = src.map(item => ({
            ot: item.ot,
            ct: item.ct,
            value: 0
        }));
        const hmaDepth = Math.floor(Math.sqrt(depth));
        const wma = wma_1.WMA.calcIndex(src, depth);
        const wma2 = wma_1.WMA.calcIndex(src, depth / 2);
        for (let i = depth - 1; i < src.length; i++) {
            hmaSrc[i].value = 2 * wma2[i].value - wma[i].value;
        }
        return wma_1.WMA.calcIndex(hmaSrc.slice(depth - 1), hmaDepth);
    }
}
exports.HMA = HMA;
//# sourceMappingURL=hma.js.map