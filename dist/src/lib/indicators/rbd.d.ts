import { IndicatorInterface, IndicatorBase } from './types';
import { Candle } from '../Candle';
export declare class IndicatorRBD extends IndicatorBase {
    constructor(data: string);
}
export interface IndicatorRBDInterface extends IndicatorInterface {
    src: number;
    rsi: number;
    sma: number;
    ema: number;
    lineUp: number;
    lineDown: number;
    dispUp: number;
    dispDown: number;
    rsiType: number;
}
export declare class RBD {
    static calc(candles: Candle[], depthRSI?: number, depthBasis?: number, devCount?: number, sigma?: number): IndicatorRBDInterface[];
    private static calcEMA;
    private static calcRSI;
}
