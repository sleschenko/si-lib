"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MACD = void 0;
const hma_1 = require("./movings/hma");
const sma_1 = require("./movings/sma");
const ema_1 = require("./movings/ema");
const wma_1 = require("./movings/wma");
const movings = {
    HMA: hma_1.HMA.calc,
    SMA: sma_1.SMA.calc,
    EMA: ema_1.EMA.calc,
    WMA: wma_1.WMA.calc,
};
class MACD {
    static calc(candles, periodFast, periodSlow, signalLength, priceType = 'close', maType = 'EMA', signalMAType = 'EMA') {
        const indicator = candles.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: 0,
            macd: 0,
            signal: 0,
            diffs: 0,
        }));
        if (candles.length < periodSlow || candles.length < periodFast) {
            return indicator;
        }
        const maFunc = movings[maType];
        const signalMAFunc = movings[signalMAType];
        const fastMA = this.padIndicatorList(maFunc(candles, periodFast, priceType), candles.length);
        const slowMA = this.padIndicatorList(maFunc(candles, periodSlow, priceType), candles.length);
        for (let i = indicator.length - 1; i >= 0; i--) {
            indicator[i].macd = fastMA[i].value - slowMA[i].value;
            indicator[i].close = indicator[i].macd;
        }
        const signal = signalMAFunc(indicator, signalLength, 'close');
        for (let i = indicator.length - 1; i >= 0; i--) {
            indicator[i].signal = signal[i].value;
            indicator[i].diffs = indicator[i].macd - indicator[i].signal;
        }
        return indicator;
    }
    static padIndicatorList(src, padTo) {
        const padLen = Math.max(padTo - src.length, 0);
        return new Array(padLen)
            .fill({
            ot: new Date(0),
            ct: new Date(0),
            value: 0,
        })
            .concat(src);
    }
}
exports.MACD = MACD;
//# sourceMappingURL=macd.js.map