"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Store = void 0;
const redis_1 = require("redis");
const EVENT_CHANNEL = 'event';
const INDICATOR_KEY_TEMPLATE = '*:indicator:*';
const CANDLE_KEY_TEMPLATE = '*:candle:*';
const KEY_TEMPLATE_BINANCE_CANDLE = 'c:binance:*:candle:*';
class Store {
    constructor(config) {
        this.eventHandlers = [];
        this.onMessage = this.onMessage.bind(this);
        this.client = redis_1.createClient(config);
        this.client.on('error', (err) => {
            console.log(`Redis error: ${err}`);
        });
        this.redisSubscriber = redis_1.createClient(config);
        this.redisSubscriber.subscribe(EVENT_CHANNEL);
        this.redisSubscriber.on('message', this.onMessage);
    }
    stop() {
        var _a, _b;
        (_a = this.client) === null || _a === void 0 ? void 0 : _a.quit();
        (_b = this.redisSubscriber) === null || _b === void 0 ? void 0 : _b.quit();
    }
    registerEventHandler(handler) {
        return this.eventHandlers.push(handler);
    }
    onMessage(channel, message) {
        this.eventHandlers.forEach(h => h(message));
    }
    sendEvent(message) {
        this.client.publish(EVENT_CHANNEL, message);
    }
    llen(key) {
        return new Promise(resolve => {
            this.client.llen(key, (err, data) => {
                resolve(err ? -1 : data);
            });
        });
    }
    deleteKey(key) {
        return new Promise(resolve => {
            this.client.del(key, (err, count) => {
                resolve(err ? 0 : count);
            });
        });
    }
    /**
     * Get keys functions
     */
    async getKeysScan(template) {
        let newCursor = '0';
        const keys = [];
        do {
            const { cursor, data } = await this.scan(newCursor, template);
            keys.push(...data);
            newCursor = cursor;
        } while (newCursor && newCursor !== '0');
        return keys;
    }
    getKeys(template) {
        return new Promise(resolve => {
            this.client.keys(template, (err, data) => {
                resolve(err ? [] : data);
            });
        });
    }
    scan(cursor, template) {
        return new Promise(resolve => {
            this.client.scan(cursor, 'MATCH', template, (err, data) => err
                ? resolve({ cursor: '', data: [] })
                : resolve({ cursor: data[0], data: data[1] }));
        });
    }
    /**
     * Get specified keys
     */
    candlesKeys() {
        return this.getKeysScan(CANDLE_KEY_TEMPLATE);
    }
    candlesBinanceKeys() {
        return this.getKeysScan(KEY_TEMPLATE_BINANCE_CANDLE);
    }
    indicatorsKeys() {
        return this.getKeysScan(INDICATOR_KEY_TEMPLATE);
    }
    /**
     * Values functions
     */
    getValues(key, start = 0, stop = -1) {
        return new Promise(resolve => {
            this.client.lrange(key, start, stop, (err, res) => {
                resolve(err ? [] : res);
            });
        });
    }
    pushValues(key, list, entityName = 'values') {
        return new Promise(resolve => {
            this.client.rpush(key, list, (err, count) => {
                if (err) {
                    resolve(0);
                }
                else {
                    this.sendEvent(`add#${entityName}#${key}`);
                    resolve(count);
                }
            });
        });
    }
    popValue(key) {
        return new Promise(resolve => this.client.rpop(key, (err, value) => resolve(err ? '' : value)));
    }
    setValue(key, index, value, entityName = 'value') {
        return new Promise(resolve => this.client.lset(key, index, value, (err, value) => {
            if (err) {
                resolve('');
            }
            else {
                this.sendEvent(`update#${entityName}#${key}`);
                resolve(value);
            }
        }));
    }
}
exports.Store = Store;
//# sourceMappingURL=Store.js.map