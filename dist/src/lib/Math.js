"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toFitPrecession = exports.addPercentToValue = exports.safeMult = exports.safeDiv = exports.safeMinus = exports.safePlus = void 0;
const bignumber_js_1 = require("bignumber.js");
function safePlus(x, y) {
    const bigX = new bignumber_js_1.BigNumber(x);
    const bigY = new bignumber_js_1.BigNumber(y);
    return bigX
        .plus(bigY)
        .toNumber();
}
exports.safePlus = safePlus;
function safeMinus(x, y) {
    const bigX = new bignumber_js_1.BigNumber(x);
    const bigY = new bignumber_js_1.BigNumber(y);
    return bigX
        .minus(bigY)
        .toNumber();
}
exports.safeMinus = safeMinus;
function safeDiv(x, y) {
    const bigX = new bignumber_js_1.BigNumber(x);
    const bigY = new bignumber_js_1.BigNumber(y);
    return bigX
        .div(bigY)
        .toNumber();
}
exports.safeDiv = safeDiv;
function safeMult(x, y) {
    const bigX = new bignumber_js_1.BigNumber(x);
    const bigY = new bignumber_js_1.BigNumber(y);
    return bigX
        .multipliedBy(bigY)
        .toNumber();
}
exports.safeMult = safeMult;
function addPercentToValue(value, percent) {
    return safePlus(value, safeMult(value, safeDiv(percent, 100)));
}
exports.addPercentToValue = addPercentToValue;
function toFitPrecession(value, precession) {
    return safeMult(Math.floor(safeDiv(value, precession)), precession);
}
exports.toFitPrecession = toFitPrecession;
//# sourceMappingURL=Math.js.map