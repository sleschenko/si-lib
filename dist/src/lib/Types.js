"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intervalMillis = void 0;
exports.intervalMillis = {
    '1m': 1000 * 60,
    '10m': 1000 * 60 * 10,
    '1h': 1000 * 60 * 60,
    '4h': 1000 * 60 * 60 * 4,
    '1d': 1000 * 60 * 60 * 24,
    '1w': 1000 * 60 * 60 * 24 * 7,
    '1M': 1000 * 60 * 60 * 24 * 30,
};
//# sourceMappingURL=Types.js.map