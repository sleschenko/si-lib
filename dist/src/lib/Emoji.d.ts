export declare const Emoji: {
    elephant: string;
    rocket: string;
    up_1: string;
    down_1: string;
    yellow_circle: string;
    green_circle: string;
    red_circle: string;
    magenta_circle: string;
    blue_circle: string;
    orange_circle: string;
    redExclamationMark: string;
    buttArrowUp: string;
    buttArrowDown: string;
};
