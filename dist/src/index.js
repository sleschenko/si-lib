"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(require("./lib/SiLib"), exports);
var Candle_1 = require("./lib/Candle");
Object.defineProperty(exports, "Candle", { enumerable: true, get: function () { return Candle_1.Candle; } });
var Emoji_1 = require("./lib/Emoji");
Object.defineProperty(exports, "Emoji", { enumerable: true, get: function () { return Emoji_1.Emoji; } });
__exportStar(require("./lib/Types"), exports);
__exportStar(require("./lib/indicators"), exports);
__exportStar(require("./lib/Math"), exports);
//# sourceMappingURL=index.js.map