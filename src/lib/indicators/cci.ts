import { Candle } from '../Candle';
import { getPriceFromType, IndicatorInterface, PriceType } from './types';
import { SMA } from './movings/sma';

export class CCI {
    /**
     * calculating CCI index
     * src algo: https://ru.wikipedia.org/wiki/%D0%98%D0%BD%D0%B4%D0%B5%D0%BA%D1%81_%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BD%D0%BE%D0%B3%D0%BE_%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%D0%B0
     */
    static calc(list: Candle[], depth: number, priceType: PriceType = 'close'): IndicatorInterface[] {
        const cci: IndicatorInterface[] = list.map(item => ({
            ot: item.openTime,
            ct: item.closeTime,
            value: 0
        }));
        const prices: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: getPriceFromType(priceType, candle),
        }));
        const pricesSMA = SMA.calcIndex(prices, depth);
        const MAD = (n: number) => {
            let sum = 0;
            for (let i = n - depth + 1; i <= n; i++) {
                sum += Math.abs(prices[i].value - pricesSMA[n].value);
            }
            return sum / depth;
        }
        for (let i = depth; i < prices.length; i++) {
            cci[i].value = (prices[i].value - pricesSMA[i].value) / (0.015 * MAD(i));
        }
        return cci;
    }

}
