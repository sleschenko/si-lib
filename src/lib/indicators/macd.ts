import { IndicatorInterface, MAType, PriceType } from './types';
import { Candle } from '../Candle';
import { HMA } from './movings/hma';
import { SMA } from './movings/sma';
import { EMA } from './movings/ema';
import { WMA } from './movings/wma';

export interface IndicatorMACDInterface extends IndicatorInterface {
    macd: number;
    signal: number;
    diffs: number;
    close?: number;
}

const movings = {
    HMA: HMA.calc,
    SMA: SMA.calc,
    EMA: EMA.calc,
    WMA: WMA.calc,
};

export class MACD {
    static calc(
        candles: Candle[],
        periodFast: number,
        periodSlow: number,
        signalLength: number,
        priceType: PriceType = 'close',
        maType: MAType = 'EMA',
        signalMAType: MAType = 'EMA',
        ): IndicatorMACDInterface[] {
        const indicator: IndicatorMACDInterface[] = candles.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: 0,
            macd: 0,
            signal: 0,
            diffs: 0,
        }));
        if (candles.length < periodSlow || candles.length < periodFast) {
            return indicator;
        }
        const maFunc: (list: Candle[], depth: number, priceType: PriceType) => IndicatorInterface[] = movings[maType];
        const signalMAFunc: (list: Candle[], depth: number, priceType: PriceType) => IndicatorInterface[] = movings[signalMAType];

        const fastMA = this.padIndicatorList(maFunc(candles, periodFast, priceType), candles.length);
        const slowMA = this.padIndicatorList(maFunc(candles, periodSlow, priceType), candles.length);
        for (let i = indicator.length - 1; i >= 0; i--) {
            indicator[i].macd = fastMA[i].value - slowMA[i].value;
            indicator[i].close = indicator[i].macd;
        }
        const signal = signalMAFunc((indicator as unknown) as Candle[], signalLength, 'close');
        for (let i = indicator.length - 1; i >= 0; i--) {
            indicator[i].signal = signal[i].value;
            indicator[i].diffs = indicator[i].macd - indicator[i].signal;
        }
        return indicator;
    }

    static padIndicatorList(src: IndicatorInterface[], padTo: number): IndicatorInterface[] {
        const padLen = Math.max(padTo - src.length, 0);
        return new Array<IndicatorInterface>(padLen)
            .fill({
                ot: new Date(0),
                ct: new Date(0),
                value: 0,
            })
            .concat(src);
    }
}
