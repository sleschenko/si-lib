import { getPriceFromType, IndicatorBase, IndicatorInterface, PriceType } from '../types';
import { Candle } from '../../Candle';

export class IndicatorEMA3 extends IndicatorBase {
    emaFast: number;
    emaMiddle: number;
    emaSlow: number;
    constructor(data: string) {
        super(data);
        this.emaFast = +this.raw.emaFast;
        this.emaMiddle = +this.raw.emaMiddle;
        this.emaSlow = +this.raw.emaSlow;
    }
}

export class EMA {
    static calc(list: Candle[], depth: number, priceType: PriceType = 'close'): IndicatorInterface[] {
        const src: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: getPriceFromType(priceType, candle),
        }));
        const ema: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: 0,
        }));
        if (depth > list.length) {
            return ema;
        }
        return EMA.calcIndex(src, ema, depth);
    }

    private static calcFirstSMA(src: IndicatorInterface[], depth: number): number {
        return src
            .slice(0, depth)
            .reduce((a, c) => a + c.value, 0) / depth;
    }

    private static calcIndex(src: IndicatorInterface[], ema: IndicatorInterface[], depth: number): IndicatorInterface[] {
        const alpha = 2 / (depth + 1);
        ema[depth - 1].value = this.calcFirstSMA(src, depth);
        for (let i = depth; i < src.length; i++) {
            ema[i].value = alpha * src[i].value + (1 - alpha) * ema[i - 1].value;
        }
        return ema;
    }

}
