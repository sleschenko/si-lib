import { Candle } from '../../Candle';
import { getPriceFromType, IndicatorInterface, PriceType } from '../types';
import { WMA } from './wma';

export class HMA {
    static calc(list: Candle[], depth: number, priceType: PriceType = 'close'): IndicatorInterface[] {
        const prices: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: getPriceFromType(priceType, candle),
        }));
        return HMA.calcIndex(prices, depth);
    }

    static calcIndex(src: IndicatorInterface[], depth: number): IndicatorInterface[] {
        const hmaSrc: IndicatorInterface[] = src.map(item => ({
            ot: item.ot,
            ct: item.ct,
            value: 0
        }));
        const hmaDepth = Math.floor(Math.sqrt(depth));
        const wma = WMA.calcIndex(src, depth);
        const wma2 = WMA.calcIndex(src, depth / 2);
        for (let i = depth - 1; i < src.length; i++) {
            hmaSrc[i].value = 2 * wma2[i].value - wma[i].value;
        }
        return WMA.calcIndex(hmaSrc.slice(depth - 1), hmaDepth);
    }
}
