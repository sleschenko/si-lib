import { Candle } from '../../Candle';
import { getPriceFromType, IndicatorInterface, PriceType } from '../types';

export class WMA {
    static calc(list: Candle[], depth: number, priceType: PriceType = 'close'): IndicatorInterface[] {
        const prices: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: getPriceFromType(priceType, candle),
        }));
        return WMA.calcIndex(prices, depth);
    }

    static calcIndex(src: IndicatorInterface[], depth: number): IndicatorInterface[] {
        const wma: IndicatorInterface[] = src.map(item => ({
            ot: item.ot,
            ct: item.ct,
            value: 0
        }));
        for (let i = depth; i <= src.length; i++) {
            wma[i - 1].value = (2 / (depth * (depth + 1))) * src
                .slice(i - depth, i)
                .reduce((a, c, num) => a + (num + 1) * c.value, 0);
        }
        return wma;
    }
}
