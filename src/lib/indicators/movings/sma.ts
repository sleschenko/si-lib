import { Candle } from '../../Candle';
import { getPriceFromType, IndicatorInterface, PriceType } from '../types';

export class SMA {
    static calc(list: Candle[], depth: number, priceType: PriceType = 'close'): IndicatorInterface[] {
        const src: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: getPriceFromType(priceType, candle),
        }));
        return SMA.calcIndex(src, depth);
    }

    static calcIndex(src: IndicatorInterface[], depth: number): IndicatorInterface[] {
        const sma: IndicatorInterface[] = src.map(item => ({
            ot: item.ot,
            ct: item.ct,
            value: 0,
        }));
        for (let i = 0; i < src.length; i++) {
            if (i < depth) {
                sma[depth - 1].value += src[i].value / depth;
            } else {
                const prevSMA = sma[i - 1].value;
                const currValue = src[i].value;
                const firstDepthValue = src[i - depth].value;
                sma[i].value = prevSMA - firstDepthValue / depth + currValue / depth;
            }
        }
        return sma;
    }

}
