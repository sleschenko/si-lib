import { Candle } from '../Candle';
import { IndicatorInterface, IndicatorBase } from './types';

export class IndicatorRSI extends IndicatorBase {
    constructor(data: string) {
        super(data);
    }
}

export class RSI {
    static calc(list: Candle[], depth: number): IndicatorInterface[] {
        const rsi: IndicatorInterface[] = list.map(candle => ({
            ot: candle.openTime,
            ct: candle.closeTime,
            value: 0,
        }));
        return this.calcRSIRecurrent(list, rsi, depth);
    }

    private static calcRSIFull(list: Candle[], rsi: IndicatorInterface[], indexDepth: number): IndicatorInterface[] {
        for (let q = list.length; q > indexDepth; q--) {
            const {grow, lose} = list
                .slice(q - indexDepth, q)
                .reduce(
                    (a, c) => {
                        if (c.growing) {
                            a.grow += c.close - c.open;
                        } else {
                            a.lose += c.open - c.close;
                        }
                        return a;
                    },
                    {grow: 0, lose: 0}
                );
            const RS = grow / lose;
            rsi[q - 1].value = 100 - 100 / (1 + RS);
        }
        return rsi;
    }

    private static calcRSIRecurrent(list: Candle[], rsi: IndicatorInterface[], indexDepth: number): IndicatorInterface[] {
        let firstGain = 0;
        let firstLose = 0;
        for (let e = 0; e < indexDepth; e++) {
            const currentPrice = list[e + 1].close;
            const prevPrice = list[e].close;
            if (currentPrice >= prevPrice) {
                firstGain += currentPrice - prevPrice;
            } else {
                firstLose += prevPrice - currentPrice;
            }
        }
        firstGain /= indexDepth;
        firstLose /= indexDepth;
        const firstRS = firstGain / firstLose;
        rsi[indexDepth].value = 100 - 100 / (1 + firstRS);

        for (let q = indexDepth + 1; q < list.length; q++) {
            const currentPrice = list[q].close;
            const prevPrice = list[q - 1].close;
            let currentGain = 0;
            let currentLose = 0;
            if (currentPrice >= prevPrice) {
                currentGain = currentPrice - prevPrice;
            } else {
                currentLose = prevPrice - currentPrice;
            }
            currentGain = (firstGain * (indexDepth - 1) + currentGain) / indexDepth;
            currentLose = (firstLose * (indexDepth - 1) + currentLose) / indexDepth;

            const currentRS = currentGain / currentLose;
            rsi[q].value = 100 - 100 / (1 + currentRS);

            firstGain = currentGain;
            firstLose = currentLose;
        }
        return rsi;
    }
}
