import { Candle } from '../Candle';
import { IndicatorInterface, IndicatorBase } from './types';

export interface IndicatorStochasticInterface extends IndicatorInterface {
    k: number;
    d: number;
}

export class IndicatorStochastic extends IndicatorBase implements IndicatorStochasticInterface {
    k: number;
    d: number;
    constructor(data: string) {
        super(data);
        this.k = +this.raw.k || 0;
        this.d = +this.raw.d || 0;
    }
}

export class Stochastic {
    static calc(list: Candle[], periodK = 14, periodD = 3, smoothK = 3, lazy = true): IndicatorStochasticInterface[] {
        if (periodK > list.length) {
            return [];
        }
        return this.calcIndex(list, periodK, periodD, smoothK, lazy);
    }

    private static calcIndex(list: Candle[], periodK: number, periodD: number, smoothK: number, lazy: boolean): IndicatorStochasticInterface[] {
        const stoch: IndicatorStochasticInterface[] = [];
        for (let i = 1; i <= (lazy ? periodK : list.length); i++) {
            const index = list.length - i;
            const currentCandle = list[index];
            const [min, max] = list
                .slice(index - periodK + 1, index + 1)
                .reduce((a, c) => [
                        c.low < a[0] ? c.low : a[0],
                        c.high > a[1] ? c.high : a[1]
                    ], [Number.MAX_VALUE, 0]);
            const value = (currentCandle.close - min) / (max - min);
            stoch.unshift({
                value,
                ot: currentCandle.openTime,
                ct: currentCandle.closeTime,
                k: 0,
                d: 0,
            });
        }
        this.calcSMA(stoch, 'k', smoothK);
        this.calcSMA(stoch, 'd', periodD);
        stoch.forEach(i => {
            i.k = Math.round(i.k * 10000) / 100;
            i.d = Math.round(i.d * 10000) / 100;
        });
        return stoch;
    }

    private static calcSMA(src: IndicatorStochasticInterface[], fieldName: 'k' | 'd', period: number): IndicatorStochasticInterface[] {
        const srcFieldName = fieldName === 'k' ? 'value' : 'k';
        for (let i = src.length; i >= period; i--) {
            src[i - 1][fieldName] = src
                .slice(i - period, i)
                .reduce((a, c) => a + c[srcFieldName], 0) / period;
        }
        return src;
    }

}
