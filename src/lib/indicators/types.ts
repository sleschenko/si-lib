import { BaseDict, CandleParams } from '../Types';
import { Candle } from '../Candle';

export type PriceType = 'open' | 'high' | 'low' | 'close' | 'hlc3';
export type MAType = 'SMA' | 'EMA' | 'HMA' | 'WMA';

export interface IndicatorParams {
    name: string;
    params: string[];
}

export interface IndicatorInterface {
    ot: Date;
    ct: Date;
    value: number;
}

export abstract class IndicatorBase implements IndicatorInterface {
    raw: BaseDict<string | number>;
    ot: Date;
    ct: Date;
    value: number;

    protected constructor(data: string) {
        this.raw = JSON.parse(data) as BaseDict<string | number>;
        const {ot, ct, value} = JSON.parse(data) as {ot: string, ct: string, value: string | number};
        this.ot = new Date(ot);
        this.ct = new Date(ct);
        this.value = +value;
    }
}

export function getIndicatorParams(params: CandleParams): IndicatorParams {
    const ps: string[] = params.options.split('-');
    return {
        name: ps[0],
        params: ps.slice(1),
    } as IndicatorParams;
}

export function getPriceFromType(priceType: PriceType, candle: Candle): number {
    switch (priceType) {
        case 'hlc3':
            return (candle.high + candle.low + candle.close) / 3;
        default:
            return candle[priceType];
    }
}
