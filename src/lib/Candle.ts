export class Candle {
    closeTime: Date;
    openTime: Date;
    open: number;
    high: number;
    low: number;
    close: number;
    volume: number;
    qty: number;
    growing: boolean;

    constructor(str: string) {
        try {
            const c: any = JSON.parse(str);
            this.closeTime = new Date(c.ct);
            this.openTime = new Date(c.ot);
            this.open = c.o;
            this.high = c.h;
            this.low = c.l;
            this.close = c.c;
            this.volume = c.v;
            this.qty = c.q;
            this.growing = this.open < this.close;
        } catch (e) {
            throw new Error('bad candle params!');
        }
    }
}
