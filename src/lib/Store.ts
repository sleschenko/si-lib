import { createClient, RedisClient } from 'redis';

const EVENT_CHANNEL = 'event';
const INDICATOR_KEY_TEMPLATE = '*:indicator:*';
const CANDLE_KEY_TEMPLATE = '*:candle:*';
const KEY_TEMPLATE_BINANCE_CANDLE = 'c:binance:*:candle:*';

export type RedisEventHandler = (msg: string) => void;

export interface RedisConfig {
    host: string;
    port: number;
}

export class Store {
    protected readonly client: RedisClient;
    private readonly redisSubscriber: RedisClient;
    private eventHandlers: RedisEventHandler[] = [];

    constructor(config: RedisConfig) {
        this.onMessage = this.onMessage.bind(this);
        this.client = createClient(config);
        this.client.on('error', (err: Error) => {
            console.log(`Redis error: ${err}`);
        });
        this.redisSubscriber = createClient(config);
        this.redisSubscriber.subscribe(EVENT_CHANNEL);
        this.redisSubscriber.on('message', this.onMessage);
    }

    stop(): void {
        this.client?.quit();
        this.redisSubscriber?.quit();
    }

    registerEventHandler(handler: RedisEventHandler): number {
        return this.eventHandlers.push(handler);
    }

    private onMessage(channel: string, message: string): void {
        this.eventHandlers.forEach(h => h(message));
    }

    public sendEvent(message: string): void {
        this.client.publish(EVENT_CHANNEL, message);
    }

    public llen(key: string): Promise<number> {
        return new Promise<number>(resolve => {
            this.client.llen(key, (err, data) => {
                resolve(err ? -1 : data);
            });
        });
    }

    deleteKey(key: string): Promise<number> {
        return new Promise(resolve => {
            this.client.del(key, (err, count) => {
                resolve(err ? 0 : count);
            });
        });
    }

    /**
     * Get keys functions
     */

    async getKeysScan(template: string): Promise<string[]> {
        let newCursor = '0';
        const keys: string[] = [];
        do {
            const {cursor, data} = await this.scan(newCursor, template);
            keys.push(...data);
            newCursor = cursor;
        } while (newCursor && newCursor !== '0');
        return keys;
    }

    getKeys(template: string): Promise<string[]> {
        return new Promise(resolve => {
            this.client.keys(template, (err, data) => {
                resolve(err ? [] : data)
            });
        });
    }

    private scan(cursor: string, template: string): Promise<{cursor: string, data: string[]}> {
        return new Promise(resolve => {
            this.client.scan(cursor, 'MATCH', template,
                (err: Error | null, data: [string, string[]]) =>
                    err
                        ? resolve({cursor: '', data: []})
                        : resolve({cursor: data[0], data: data[1]})
            )
        });
    }

    /**
     * Get specified keys
     */

    candlesKeys(): Promise<string[]> {
        return this.getKeysScan(CANDLE_KEY_TEMPLATE);
    }

    candlesBinanceKeys(): Promise<string[]> {
        return this.getKeysScan(KEY_TEMPLATE_BINANCE_CANDLE);
    }

    indicatorsKeys(): Promise<string[]> {
        return this.getKeysScan(INDICATOR_KEY_TEMPLATE);
    }

    /**
     * Values functions
     */

    getValues(key: string, start = 0, stop = -1): Promise<string[]> {
        return new Promise(resolve => {
            this.client.lrange(key, start, stop, (err, res: string[]) => {
                resolve(err ? [] : res);
            });
        });
    }

    pushValues(key: string, list: string[], entityName = 'values'): Promise<number> {
        return new Promise(resolve => {
            this.client.rpush(key, list, (err, count: number) => {
                if (err) {
                    resolve(0);
                } else {
                    this.sendEvent(`add#${entityName}#${key}`);
                    resolve(count);
                }
            });
        });
    }

    popValue(key: string): Promise<string> {
        return new Promise(resolve =>
            this.client.rpop(key, (err, value: string) => resolve(err ? '' : value))
        );
    }

    setValue(key: string, index: number, value: string, entityName = 'value'): Promise<string> {
        return new Promise(resolve =>
            this.client.lset(key, index, value,
                (err, value: string) => {
                    if (err) {
                        resolve('');
                    } else {
                        this.sendEvent(`update#${entityName}#${key}`);
                        resolve(value);
                    }
                }
            )
        );
    }
}
