import { BigNumber } from 'bignumber.js';

export function safePlus(x: number, y: number): number {
    const bigX = new BigNumber(x);
    const bigY = new BigNumber(y);
    return bigX
        .plus(bigY)
        .toNumber();
}

export function safeMinus(x: number, y: number): number {
    const bigX = new BigNumber(x);
    const bigY = new BigNumber(y);
    return bigX
        .minus(bigY)
        .toNumber();
}

export function safeDiv(x: number, y: number): number {
    const bigX = new BigNumber(x);
    const bigY = new BigNumber(y);
    return bigX
        .div(bigY)
        .toNumber();
}

export function safeMult(x: number, y: number): number {
    const bigX = new BigNumber(x);
    const bigY = new BigNumber(y);
    return bigX
        .multipliedBy(bigY)
        .toNumber();
}

export function addPercentToValue(value: number, percent: number): number {
    return safePlus(value, safeMult(value, safeDiv(percent, 100)));
}

export function toFitPrecession(value: number, precession: number): number {
    return safeMult(Math.floor(safeDiv(value, precession)), precession);
}
