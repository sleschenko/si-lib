import { CandleInterval, CandleParams, EntityType, StockExchange, StockExchangeType } from './Types';

export function info(): string{
    return 'SafeInvest base library';
}

export async function sleep(timeout: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => resolve(), timeout));
}

export function getParamsFromKey(key: string): CandleParams {
    const params = key.split(':');
    return {
        seType: params[0] as StockExchangeType,
        se: params[1] as StockExchange,
        symbol: params[2],
        interval: params[3] as CandleInterval,
        entityType: params[4] as EntityType,
        options: params[5],
        key,
    };
}

export function keyFromParams(params: CandleParams): string {
    return `${params.seType}:${params.se}:${params.symbol}:${params.interval}:${params.entityType}:${params.options}`;
}

export function pad2Length(src = '', length = 10): string {
    return src + ' '.repeat(length - src.length);
}
