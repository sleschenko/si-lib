export type BaseKey = string | number;
export type BaseDict<T = string> = {[key in BaseKey]: T};

export type StockExchangeType = 'c' | 'f';
export type StockExchange = 'binance' | 'moex' | 'tsc';
export type EntityType = 'candle' | 'open' | 'indicator';
export type CandleInterval = '1m' | '10m' | '1h' | '4h' | '1d' | '1w' | '1M';

export interface CandleParams {
    seType: StockExchangeType;
    se: StockExchange;
    symbol: string;
    interval: CandleInterval;
    entityType: EntityType;
    options: string;
    key: string;
}

export const intervalMillis: {[key in CandleInterval]: number} = {
    '1m': 1000 * 60,
    '10m': 1000 * 60 * 10,
    '1h': 1000 * 60 * 60,
    '4h': 1000 * 60 * 60 * 4,
    '1d': 1000 * 60 * 60 * 24,
    '1w': 1000 * 60 * 60 * 24 * 7,
    '1M': 1000 * 60 * 60 * 24 * 30,
}
